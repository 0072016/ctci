package Question9_2;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;

import CtCILibrary.AssortedMethods;

/**
 * Iterative, DP solution for Question 9.2
 *
 * @author José Ángel Soler Ortiz
 *
 */
public class QuestionIterativeDP {
	// Maze size
	static int size = 10;

	// If 0, the coordinate is off-limits.
	static int[][] maze;

	// Cache of already explored points.
	static HashSet<Point> expPoints = new HashSet<>();

	// Determines if a path should be added to the exploration queue. Keeping paths in memory is quite expensive so it should be avoided. 
	public static boolean isValid(Point point) {
		return point.x < maze.length && point.y < maze[0].length && maze[point.x][point.y] != 0 && !expPoints.contains(point);
	}

	public static boolean isSolution(Point point) {
		return (point.x == (maze.length - 1)) && (point.y == (maze[0].length - 1));
	}

	public static void getPath() {
		// Depth first exploration. Right first, then down.
		Stack<ArrayList<Point>> explorationPaths = new Stack<>();
		ArrayList<Point> originalPath = new ArrayList<>();
		// Start exploring at (0, 0).
		originalPath.add(new Point(0, 0));
		// Add path to exploration queue.
		explorationPaths.add(originalPath);

		ArrayList<Point> solution = null;

		while (!explorationPaths.isEmpty() && solution == null) {
			ArrayList<Point> current = explorationPaths.pop();
			Point lastPoint = current.get(current.size() - 1);

			if (isSolution(lastPoint)) {
				solution = current;
				explorationPaths.clear();
			} else {
				// Explore down.
				Point downPoint = new Point(lastPoint.x, lastPoint.y + 1);
				if (isValid(downPoint)) {
					// Create the new path.
					ArrayList<Point> down = new ArrayList<>();
					// Reuse the previous points (saves up some memory).
					down.addAll(current);
					down.add(downPoint);
					// Add it to exploration stack.
					explorationPaths.add(down);
					// The same point should NOT be explored twice.
					expPoints.add(downPoint);
				}
				// Explore right.
				Point rightPoint = new Point(lastPoint.x + 1, lastPoint.y);
				if (isValid(rightPoint)) {
					// Create the new path.
					ArrayList<Point> right = new ArrayList<>();
					// Reuse the previous points (saves up some memory).
					right.addAll(current);
					right.add(rightPoint);
					// Add it to exploration stack.
					explorationPaths.add(right);
					// The same point should NOT be explored twice.
					expPoints.add(rightPoint);
				}
			}
		}

		if (solution == null) {
			System.out.println("No path exists.");
		} else {
			String s = AssortedMethods.listOfPointsToString(solution);
			System.out.println("Path: " + " " + s);
		}
	}

	// Worst case scenario for the algorithm.
	public static void forceDeadEndDown() {
		for (int i = 1; i <= size - 1; i++) {
			maze[size - i][size - 2] = 0;
			maze[size - i][size - 1] = 1;
		}
	}
	public static void main(String[] args) {
		maze = AssortedMethods.randomMatrix(size, size, 0, 5);
		//forceDeadEndDown();
		AssortedMethods.printMatrix(maze);
		getPath();
	}
}
