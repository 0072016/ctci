package Question9_6;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Iterative, DP solution for Question 9.6
 *
 * @author José Ángel Soler Ortiz
 *
 */
public class QuestionIterative {

	class State {
		String seq;
		int numOpened;
		int numClosed;

		State() {
			seq = "";
			numOpened = 0;
			numClosed = 0;
		}

		State(State orig, boolean open) {
			seq = new String(orig.seq);
			numOpened = orig.numOpened;
			numClosed = orig.numClosed;
			if (open) {
				seq += "(";
				numOpened++;
			} else {
				seq += ")";
				numClosed++;
				numOpened--;
			}
		}
	}

	QuestionIterative() {
	
	}

	public ArrayList<String> getParentheses(int pairs) {
		ArrayList<String> sols = new ArrayList<>();
		Stack<State> expl = new Stack<>();
		expl.push(new State());

		while (!expl.isEmpty()) {
			State curr = expl.pop();
			if (curr.numClosed == pairs) {
				sols.add(curr.seq);
			} else {
				if (curr.numOpened > 0) {
					// Close one parenthesis.
					expl.push(new State(curr, false));
				}
				if ((curr.numOpened + curr.numClosed) < pairs) {
					// Open parenthesis.
					expl.push(new State(curr, true));
				}
			}
		}

		return sols;
	}

	public static void main(String[] args) {
		QuestionIterative q = new QuestionIterative();
		ArrayList<String> result = q.getParentheses(10);
		for (String seq : result) {
			System.out.println(seq);
		}
		System.out.println(result.size());
	}
}
