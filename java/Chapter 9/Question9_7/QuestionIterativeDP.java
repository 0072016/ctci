package Question9_7;

import java.awt.Point;
import java.util.HashSet;
import java.util.Stack;

/**
 * Iterative, DP solution for Question 9.7
 *
 * @author José Ángel Soler Ortiz
 *
 */
public class QuestionIterativeDP {

	public enum Color {
		Black, White, Red, Yellow, Green
	}
	
	public static String PrintColor(Color c) {
		switch(c) {
		case Black:
			return "B";
		case White:
			return "W";
		case Red:
			return "R";
		case Yellow:
			return "Y";
		case Green:
			return "G";
		}
		return "X";
	}
	
	public static void PrintScreen(Color[][] screen) {
		for (int i = 0; i < screen.length; i++) {
			for (int j = 0; j < screen[0].length; j++) {
				System.out.print(PrintColor(screen[i][j]));
			}
			System.out.println();
		}
	}
	
	public static int randomInt(int n) {
		return (int) (Math.random() * n);
	}
	
	public static void PaintFill(Color[][] screen, int x, int y, Color ncolor) {
		Color ocolor = screen[y][x];
		Point start = new Point(y, x);
		Stack<Point> expl = new Stack<>();
		HashSet<Point> done = new HashSet<>();
		expl.push(start);

		while (!expl.isEmpty()) {
			Point curr = expl.pop();
			if (curr.x < 0 || curr.x >= screen[0].length ||
					curr.y < 0 || curr.y >= screen.length ||
					!screen[curr.y][curr.x].equals(ocolor) || done.contains(curr)) {
				continue;
			}
			done.add(curr);
			screen[curr.y][curr.x] = ncolor;
			expl.push(new Point(curr.x - 1, curr.y));
			expl.push(new Point(curr.x + 1, curr.y));
			expl.push(new Point(curr.x, curr.y - 1));
			expl.push(new Point(curr.x, curr.y + 1));
		}
	}
	
	public static void main(String[] args) {
		int N = 10;
		Color[][] screen = new Color[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				screen[i][j] = Color.Black;
			}			
		}
		for (int i = 0; i < 100; i++) {
			screen[randomInt(N)][randomInt(N)] = Color.Green;
		}
		PrintScreen(screen);
		PaintFill(screen, 2, 2, Color.White);
		System.out.println();
		PrintScreen(screen);
	}

}
